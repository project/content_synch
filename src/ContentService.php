<?php

namespace Drupal\content_synch;
use Drupal\Core\Field\EntityReferenceFieldItemList;

Class ContentService {
  public function content_push($entity) {
    if ($entity->getEntityTypeId() == 'node') {
      $configured_states = [];
      $config_obj = \Drupal::config('content_synch.config');
      $configured_states= [
        $config_obj->get('publish_state'),
        $config_obj->get('archive_state'),
      ];
      if (in_array($entity->moderation_state->value, $configured_states)) {
        $content_insert = $this->push_dependent_entity($entity);
      }      
    }
  }

  public function push_dependent_entity($entity) {
    $entity_type = $entity->getEntityTypeId();
    $entity_uuid = $entity->uuid->value;
    if ($entity_type != 'user') {
      $content_type_fields = [
        'uid',
        'field_tags',
        'field_product_image',
        'field_product_category',
        'field_product_media_items',
        'field_product_subcategory',
        'field_banner_image',
        'field_card_image',
        'field_image',
        'field_media',
        'field_media_image',
        'thumbnail',
        'field_media_audio_file',
        'field_media_video_file',
      ];

      $paragraph_fields = [
        'field_media',
        'field_product_media_items',
        'field_product_subcategory',
      ];
      foreach ($content_type_fields as $field_name) {
        if ($entity->hasField($field_name)) {
          if ($entity->get($field_name) && $entity->get($field_name) instanceof EntityReferenceFieldItemList) {
            if ($dependent_entity = $entity->get($field_name)->referencedEntities()) {
              foreach ($dependent_entity as $key => $entity_name) {
                $entity_response_data = $this->push_dependent_entity($entity_name);
                if (in_array($field_name, $paragraph_fields) ) { 
                  if (isset($entity_response_data['revision_id'])) {
                    $entity->$field_name[$key]->target_revision_id = $entity_response_data['revision_id'][0]['value'];
                  }
                  if (isset($entity_response_data['id'])) {
                    $entity->$field_name[$key]->target_id = $entity_response_data['id'][0]['value'];
                  }                    
                }              
              }
            }
          }
        }      
      }
      
      $api_method = 'post';
      $entity_id_value = $this->entity_availability_check($entity_type, $entity_uuid);
      if (!empty($entity_id_value)) {
        $api_method = 'patch';
      }
      $rest_api_url = $this->construct_api_url($entity_type, $api_method, $entity_id_value);
      $store_entity_id = 0;
      switch ($entity_type) {
        case 'paragraph';
          $store_entity_id = $entity->id[0]->value;
        break;
        case 'taxonomy_term';
          $store_entity_id = $entity->tid[0]->value;
        break; 
        case 'file';
          $store_entity_id = $entity->fid[0]->value;
        break;         
        case 'media';
          $store_entity_id = $entity->mid[0]->value;
        break;    
        case 'node';
          $store_entity_id = $entity->nid[0]->value;
        break;  
      }
      //  Get the current timestamp value.
      $api_trigger_time = $this->get_current_time();   

      $log_tbl_insert_array = array(
        'entity_type' => ''.$entity_type,
        'entity_id' => $store_entity_id,
        'entity_uuid' => ''.$entity->uuid[0]->value,
        'api_method' => ''.$api_method,
        'api_status' => 'in progress', 
        'start_time' => ''.$api_trigger_time,
      ); 

      $serialized_api_payload = $this->construct_api_payload($entity, $entity_type); 

      if ($entity_type == 'file') {
        return TRUE;
      }

      $payload['body'] = $serialized_api_payload;
     
      $log_table_primary_key = $this->insert_table_data($log_tbl_insert_array, 'content_synch_log');

      $api_response = $this->rest_api_call_fn($rest_api_url, $payload, $api_method, $log_table_primary_key, $entity_type);

      if ($entity_type == 'node' && isset($api_response) && isset($api_response['nid'])) {
        if ($api_response['nid'][0]['value'] != '') {
          $node_moderation_state = $entity->moderation_state->value;
          $update_api_payload = array(
            'moderation_state' => $node_moderation_state,          
            'status' => $entity->status->value,          
          );
          $update_api_url = '/custom/content-synch/node-moderation-state/'.$api_response['nid'][0]['value'];
          $update_payload['body'] = json_encode($update_api_payload);
          $patch_response = $this->rest_api_call_fn($update_api_url, $update_payload, 'patch');
        }
      }
      return $api_response;
    } else {
      return [];
    }
  }

  
  public function entity_availability_check($entity_type, $entity_uuid) {
    $api_url = '/custom/content-synch/get-entity/'.$entity_type.'/'.$entity_uuid;
    $api_response = $this->rest_api_call_fn($api_url);
    if (isset($api_response['entity_id'])) {
      return $api_response['entity_id'];
    } else {
      return '';
    }
  }

  
  public function construct_api_url($entity_type, $api_method, $entity_id) {
    switch ($entity_type) {
      case 'paragraph':
        $api_url = '/entity/paragraph';
        if ($api_method == 'patch') {
          $api_url = '/entity/paragraph/' . $entity_id;
        }
        break;
      case 'taxonomy_term':
        $api_url = '/taxonomy/term';
        if ($api_method == 'patch') {
          $api_url = '/taxonomy/term/' . $entity_id;
        }
        break;
      case 'media':
        $api_url = '/entity/media';
        if ($api_method == 'patch') {
          $api_url = '/media/' . $entity_id . '/edit';
        }
        break;
      case 'file':
        $api_url = '/entity/file';
        if ($api_method == 'patch') {
          $api_url = '/entity/file/' . $entity_id;
        }
        break;
      case 'node':
        $api_url = '/node';
        if ($api_method == 'patch') {
          $api_url = '/node/' . $entity_id;
        }
      break;
    }
    return $api_url;
  }

  
  public function rest_api_call_fn($api_url, $api_payload = [], $api_method = 'get', $log_table_primary_key = '', $entity_type = '') {
    $api_response = [];
    $config_obj = \Drupal::config('content_synch.config');
    $api_url = rtrim($config_obj->get('target_url'), '/') . $api_url;
    $api_url = rtrim($api_url, '/') . '?_format=json';
    $api_payload['headers'] = [
      'Content-Type' => 'application/json'
    ];

    if ($config_obj->get('username') && $config_obj->get('password')) {
      $api_payload['auth'] = [
        $config_obj->get('username'),
        $config_obj->get('password')
      ];
    }
    
    $rest_api_status = 'in progress';
    $api_error_response = NULL;

    try {
      if ($api_method == 'get') {
        $api_request =  \Drupal::httpClient()->get($api_url, $api_payload);
      } elseif ($api_method == 'post') {
        $api_request =  \Drupal::httpClient()->post($api_url, $api_payload);
      } elseif ($api_method == 'patch') {
        $api_request =  \Drupal::httpClient()->patch($api_url, $api_payload);
      }      
      $api_response = json_decode($api_request->getBody(), TRUE);
      $rest_api_status = 'success';
    } catch (\Exception $e) {
      $rest_api_status = 'fail';
      $api_error_response = $e->getMessage();
      \Drupal::logger('content-synch')->error($e->getMessage());
    }
  
    if ($log_table_primary_key != '') {
      $api_req_completed_timestamp = $this->get_current_time();   
      $target_env_entity_id = 0;
      switch ($entity_type) {
        case 'paragraph';
          if (isset($api_response->id[0]->value)) {
            $target_env_entity_id = $api_response->id[0]->value;
          }          
        break;
        case 'taxonomy_term';
          if (isset($api_response->tid[0]->value)) {
            $target_env_entity_id = $api_response->tid[0]->value;
          }  
        break;    
        case 'media';
          if (isset($api_response->mid[0]->value)) {
            $target_env_entity_id = $api_response->mid[0]->value;
          }
        break;    
        case 'file';
          if (isset($api_response->fid[0]->value)) {
            $target_env_entity_id = $api_response->fid[0]->value;
          }
        break;
      case 'node';
        if (isset($api_response->nid[0]->value)) {
          $target_env_entity_id = $api_response->nid[0]->value;
        } 
        break;
      }

      $update_log_tbl_array = array(      
        'api_status' => ''.$rest_api_status,
        'target_env_entity_id' => $target_env_entity_id,
        'end_time' => ''.$api_req_completed_timestamp,
        'error_description' => $api_error_response,
      );
      
      $update_log_table = $this->update_table_data($update_log_tbl_array, 'content_synch_log', $log_table_primary_key);
    }
    return $api_response;
  }


  public function insert_table_data($tbl_values, $table_name) {
    $table_primary_key = \Drupal::database()->insert(''.$table_name)->fields($tbl_values)->execute();
    return $table_primary_key;
  }


  public function update_table_data($tbl_values, $table_name, $primary_key) {
    $table_row_update = \Drupal::database()->update(''.$table_name)
      ->fields($tbl_values)
      ->condition('id', $primary_key)
      ->execute();
    return $table_row_update;
  }


  public function get_current_time() {
      $current_time = time();
      return $current_time;
  }


  public function construct_api_payload($entity, $entity_type) {

    $load_serializer = \Drupal::service('serializer');
    $serialized_data = $load_serializer->serialize($entity, 'json', ['plugin_id' => 'entity']);
    $entity_data = json_decode($serialized_data);

    unset($entity_data->changed);
    switch ($entity_type) {
      case 'paragraph';
       unset($entity_data->id);
       unset($entity_data->target_id);
       unset($entity_data->revision_id);       
      break;  
      case 'taxonomy_term';
        unset($entity_data->tid);
        unset($entity_data->revision_id);
      break;
      case 'media';
        unset($entity_data->mid);
        unset($entity_data->vid);
        unset($entity_data->revision_created);
      break;    
      case 'file';
        $host_data = \Drupal::request()->getSchemeAndHttpHost();
        $file_array = [];
        $file_array['filename'] = $entity_data->filename[0]->value;
        $file_array['file_url'] = rtrim($host_data, '/') . $entity_data->uri[0]->url;
        $file_array['wrapper'] = $entity_data->uri[0]->value;        
        $file_array['uuid'] = $entity_data->uuid[0]->value;
        $this->custom_file_push($file_array);
      break;
      case 'node'; 
        unset($entity_data->nid);        
        unset($entity_data->vid);
        unset($entity_data->uid);
        unset($entity_data->moderation_state);     
        unset($entity_data->status);
        unset($entity_data->revision_timestamp);
        unset($entity_data->revision_uid);
        unset($entity_data->revision_translation_affected);
        unset($entity_data->revision_log);
      break;    
    }
    if(isset($entity_data->moderation_state)) {
      unset($entity_data->moderation_state);
    }    
    $serialized_data = json_encode($entity_data);        
    return $serialized_data;
  }


  public function custom_file_push($file_array) {
    if (!empty($file_array)) {  
      $api_url = '/custom/content-synch/file';
      $api_payload = ['body' => json_encode($file_array)];
      $api_response = $this->rest_api_call_fn($api_url, $api_payload, 'post');
      return $api_response;
    }
  }
  
}