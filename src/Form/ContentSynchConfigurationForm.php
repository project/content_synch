<?php

namespace Drupal\content_synch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Env Bridge Content Push settings.
 */
class ContentSynchConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_synch_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_synch.config',
    ];
  }

  /**
   * Get a list of content types.
   */
  function get_content_types() {
    $content_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $type_names = array();
    foreach ($content_types as $type) {
      $type_names[$type->id()] = $type->label();
    }
    return $type_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $content_type_options = $this->get_content_types();
    $config_obj = $this->config('content_synch.config');

    $form['target_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the target environment to push content'),
      '#description' => $this->t('Enter the target environment to push content.'),
      '#default_value' => $config_obj->get('target_url'),
      '#required' => TRUE,
    ];
    $form['selected_content_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Select content types'),
      '#description' => $this->t('Select the content types.'),
      '#multiple' => TRUE,
      '#options' => $content_type_options,
      '#default_value' => $config_obj->get('selected_content_types') ?: [],
      '#required' => TRUE,
    ];
    $form['publish_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the state to push content to target environment'),
      '#description' => $this->t('Enter the state to push content to target environment.'),
      '#default_value' => $config_obj->get('publish_state'),
      '#required' => TRUE,
    ];
    $form['archive_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the state to archive content to target environment'),
      '#description' => $this->t('Enter the state to archive content to target environment'),
      '#default_value' => $config_obj->get('archive_state'),
    ];
    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enter Auth details'),
    ];
    $form['auth']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the username'),
      '#description' => $this->t('Enter the username.'),
      '#default_value' => $config_obj->get('username'),
      '#required' => TRUE,
    ];
    $form['auth']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Enter the password'),
      '#description' => $this->t('Enter the password.'),
      '#default_value' => $config_obj->get('password'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('content_synch.config')
      ->set('target_url', $values['target_url'])
      ->set('selected_content_types', $values['selected_content_types'])
      ->set('publish_state', $values['publish_state'])
      ->set('archive_state', $values['archive_state'])
      ->set('username', $values['username'])
      ->set('password', $values['password'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
