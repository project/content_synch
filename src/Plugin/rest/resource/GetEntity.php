<?php

namespace Drupal\content_synch\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\ModifiedResourceResponse;

/**
 * Provides a resource for entity.
 *
 * @RestResource(
 *   id = "custom_content_synch_get_entity",
 *   label = @Translation("Get the entity status from the target environment."),
 *   uri_paths = {
 *     "canonical" = "/custom/content-synch/get-entity/{type}/{uuid}"
 *   }
 * )
 */
class GetEntity extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @param string $type
   *   The type of entity.
   * @param string $uuid
   *   The uuid of entity.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   */
  public function get($type = NULL, $uuid = NULL) {
    $result = [];
    if ($type && $uuid) {     
      $entity = \Drupal::service('entity.repository')->loadEntityByUuid($type, $uuid);
      if ($entity instanceof EntityInterface) {
        $result['entity_id'] = $entity->id();      
      }
    }
    return new ModifiedResourceResponse($result, 200);
  }

}
