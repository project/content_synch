<?php

namespace Drupal\content_synch\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\content_moderation\Entity\ModerationState;

/**
 * Provides a resource to update a node moderation state through REST.
 *
 * @RestResource(
 *   id = "custom_content_synch_update_node_moderation_state",
 *   label = @Translation("Update the node workflow moderation status."),
 *   uri_paths = {
 *     "canonical" = "/custom/content-synch/node-moderation-state/{nid}"
 *   }
 * )
 */
class UpdateNodeModeration extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  public function patch($nid = NULL, $data = []) {
    if ($nid && $data) {
      $node_obj = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
      if ($node_obj) {
        $node_moderation_state_payload = $data['moderation_state'];
        $node_status_payload = $data['status'];
        $config_obj = \Drupal::config('content_synch.config');
        $push_state_config = $config_obj->get('publish_state');
        $push_archived_config = $config_obj->get('archive_state');
        $set_status = true;
        $set_moderation_state = 'published';
        if ($push_archived_config == $node_moderation_state_payload) {
          $set_status = false;
          $set_moderation_state = 'archived';
        }
        if ($push_state_config == $node_moderation_state_payload) {
          $set_status = true;
          $set_moderation_state = 'published';
        }
        if ($node_obj->hasField('status') && $node_obj->hasField('moderation_state')) {
          $node_obj->set('status', $set_status);
          $node_obj->set('moderation_state', $set_moderation_state);
          $node_obj->save();
          return new ModifiedResourceResponse($node_obj->toArray(), 200);
        } else {
          \Drupal::logger('content-synch')->error("Status or Moderation status field is missing.");
          return new ModifiedResourceResponse(['error' => 'Status or Moderation status field is missing.'], 404);
        }
      }
      \Drupal::logger('content-synch')->error("Node not found or not published.");
      return new ModifiedResourceResponse(['error' => 'Node not found or not published'], 404);
    }
    \Drupal::logger('content-synch')->error("The server was unable to process the request because it contains invalid data.");
    return new ModifiedResourceResponse(['error' => 'The server was unable to process the request because it contains invalid data'], 422);
  }
}
