<?php

namespace Drupal\content_synch\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

/**
 * Provides a resource to save the file entity.
 * @RestResource(
 *   id = "custom_content_synch_file_push",
 *   label = @Translation("Custom Content Synch File Push"),
 *   uri_paths = {
 *     "create" = "/custom/content-synch/file"
 *   }
 * )
 */
class CustomFilePush extends ResourceBase {

  use StringTranslationTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest_examples'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Creates a new node.
   *
   * @param mixed $data
   *   Data to create the node.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $data = json_decode(json_encode($data), TRUE);
    if (!empty($data['file_url']) && !empty($data['wrapper'])) {
      // Create or Update file.
      $file_system = \Drupal::service('file_system');
      $directory_value = explode('/', $data['wrapper']);
      $directory_array_size = sizeof($directory_value) - 1;
      $directory_path = str_replace($directory_value[''.$directory_array_size], '', $data['wrapper']);

      if (!file_exists($directory_path)) {
        if(!$file_system->prepareDirectory($directory_path, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          // Log an error or handle the failure accordingly.
          \Drupal::logger('content-synch')->error("Failed to create directory at: $directory_path");
          return new ResourceResponse("Failed to create directory at: $directory_path", 500);
        }        
      }
      
      $file_repository = \Drupal::service('file.repository');
      $file_path = ''.$directory_path.$data['filename'];
      $file = $file_repository->writeData(file_get_contents($data['file_url']), $file_path, FileSystemInterface::EXISTS_REPLACE);

      // Update file entity info.
      if ($data['uuid']) {
        $entity = \Drupal::service('entity.repository')->loadEntityByUuid('file', $data['uuid']);
        if ($entity instanceof EntityInterface) {
          $file = File::load($entity->id());
          $file->set('filename', $data['filename']);
          $file->set('uri', $data['wrapper']);
          $file->set();
          $message = $this->t("File updated from @file_path to the @current_path ", [
            '@file_path' => $data['file_url'],
            '@current_path' => $file_path,
          ]);
          return new ResourceResponse($message, 200);
        }
        
        $file->set('uuid', $data['uuid']);
        $file->save();
        $message = $this->t("File copied from @file_path  to the @current_path ", [
          '@file_path' => $data['file_url'],
          '@current_path' => $file_path,
        ]);
        return new ResourceResponse($message, 200);
      } else {
        $message = $this->t("File uuid is missing in API payload");
        \Drupal::logger('content-synch')->error("File uuid is missing in API payload.");
        return new ResourceResponse($message, 422);
      }
    } else {
      $message = $this->t("File URL is missing in API payload");
      \Drupal::logger('content-synch')->error("File URL is missing in API payload.");
      return new ResourceResponse($message, 422);
    }
  } 

}