Content Synch
===========

This module provides a user interface to push the content to target environment.

Installation
===========

Once the module has been installed, navigate to /admin/config/content-synch/form
(Configuration > Content Synch > Content Synch Configuration Form administration panel) and configure the content synch.


------------------------------

Configuration Form URL:
<your domain>/admin/config/content-synch/form

The configuration form have below fields:

Field name: Enter the target environment to push content
This field value should be the target environment/instance URL.

Field name: Select content types
This field value should be the content type list. This feature or option are only applicable for selected content types.

Field name: Enter the state to push data to target environment
This field value should be the workflow state that used to publish the content in the target environment.

Field name: Enter the state to archive data to target environment
This field value should be the workflow state that used to archive the content in the target environment.

Field name: Enter the username
This field value should be the API auth username to access the target environment REST API.

Field name: Enter the password
This field value should be the API auth password to access the target environment REST API.


Note: Make sure the given auth user is available in the target environment. Also that user have access to create a content in the target environment.

------------------------------

If paragraphs item should pushed to the target environment, enable the paragraphs contributed module and paragraph REST.

Paragraphs contributed module:
https://www.drupal.org/project/paragraphs


Paragraph REST:
/entity/paragraph/{paragraph}: GET, PATCH
/entity/paragraph: POST


------------------------------

If media items should pushed to the target environment, enable the media module and media REST.

Media REST: 
/media/{media}/edit: GET, PATCH
/entity/media: POST


------------------------------


To Do:
1. Make the content type fields as dynamic in ContentService.php file.
2. Option to enale feature for required content types
3. Restrict editing content on state change of Prom to prod and Archive inProd.
4. Exception handling in file-push REST.
5. Review and restruct the code in filepush.php from 111 to 117.
6. Processing failed items using cron.

------------------------------


